// ======================================================================
// OPTIONS
// ======================================================================

const enabled = true;
const debounce = 100;

// ======================================================================
// DATA
// ======================================================================

var width1 = 0;
var height1 = 0;
var ratio1 = 0;
var width2 = 0;
var height2 = 0;
var ratio2 = 0;

var changedWidthArray = [];
var changedHeightArray = [];
var changedAnyArray = [];

var timeout = null;

// ======================================================================
// INIT
// ======================================================================

$(document).ready(function () {
  run(true);
});
$(window).on('load', function () {
  run(true);
});
$(window).resize(function () {
  run();
});

// ======================================================================
// RUN
// ======================================================================

// debounce function to stop resize events firing too frequently
const run = (force) => {
  if (enabled == true) {
    clearTimeout(timeout);
    if (force == true) {
      resize(force);
    } else {
      timeout = setTimeout(function () {
        resize(force);
      }, debounce);
    }
  }
};

const resize = (force) => {
  console.log('%c--- windowsize ---', 'color:#DDD6AA');
  // get old values
  let oldWidth1 = width1;
  let oldHeight1 = height1;
  let oldWidth2 = width2;
  let oldHeight2 = height2;
  // get and save new values
  width1 = $(window).width();
  height1 = $(window).height();
  ratio1 = width1 / height1;
  width2 = window.innerWidth ? window.innerWidth : width1;
  height2 = window.innerHeight ? window.innerHeight : height1;
  ratio2 = width2 / height2;
  // compare values
  if (force == true) {
    changedWidth();
    changedHeight();
    changedAny();
  } else {
    if (width1 != oldWidth1 || width2 != oldWidth2) {
      changedWidth();
    }
    if (height1 != oldHeight1 || height2 != oldHeight2) {
      changedHeight();
    }
    if (width1 != oldWidth1 || width2 != oldWidth2 || height1 != oldHeight1 || height2 != oldHeight2) {
      changedAny();
    }
  }
};

// ======================================================================
// CALLBACKS
// ======================================================================

const changedWidth = () => {
  for (let i in changedWidthArray) {
    changedWidthArray[i]();
  }
};
const changedHeight = () => {
  for (let i in changedHeightArray) {
    changedHeightArray[i]();
  }
};
const changedAny = () => {
  for (let i in changedAnyArray) {
    changedAnyArray[i]();
  }
};

// ======================================================================
// ADD CALLBACKS
// ======================================================================

export const onChangedWidth = (func) => {
  if (typeof func === 'function') {
    changedWidthArray.push(func);
  }
};
export const onChangedHeight = (func) => {
  if (typeof func === 'function') {
    changedHeightArray.push(func);
  }
};
export const onChanged = (func) => {
  if (typeof func === 'function') {
    changedAnyArray.push(func);
  }
};
