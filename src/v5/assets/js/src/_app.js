// ======================================================================
// IMPORTS
// ======================================================================

import * as windowsize from './windowsize';
import * as utils from './utils';
import ajax from './ajax';

// ======================================================================
// EXAMPLES
// ======================================================================

utils.localStorageSet('test', 'test2');

$(document).ready(function () {
  $('#scrollToTop').click(function () {
    utils.scrollToTop(500);
  });
  $('#scrollToPosition').click(function () {
    utils.scrollToPosition(30, 500);
  });
});

// windowsize.onChanged(() => {
//   console.log('resized');
// });
// windowsize.onChangedWidth(() => {
//   console.log('resized width');
// });
// windowsize.onChangedHeight(() => {
//   console.log('resized height');
// });

// ajax({
//   type: 'get',
//   url: 'https:// www.googleapis.com/youtube/v3/search',
//   data: {
//     q: 'test'
//   },
//   dataType: 'jsonp',
//   callback: (response) => {
//     console.log(response);
//   }
// });
