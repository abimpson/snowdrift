// ======================================================================
// AJAX
// ======================================================================

const ajax = (opts) => {
  // DEFAULT OPTIONS
  var options = {
    // ajax options
    headers: {}, // headers // {'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
    method: 'POST', // ajax method (POST or GET)
    url: '', // url to fetch
    data: {}, // parameters to pass
    dataType: 'json', // format of data beign fetched (json,xml etc)
    timeout: 20000, // ajax function timeout limit
    crossdomain: true, // cross domain?
    async: true, // async?
    callback: function () {}, // callback function after AJAX
    callbackVars: false, // additional vars to pass to the callback function
    // other options
    showErrors: true, // show errors
    logErrors: false, // log errors to database
    encrypt: false, // encrypt ajax call?
    error: false, // is this an error log?
    // data stores
    retry: 0, // stores how many times the query has been retried (don't edit this)
    limit: 2 // stores how many times to retry after a timeout
  };

  // EXTEND OPTIONS
  $.extend(options, opts);

  // ENCRYPTION
  if (options.encrypt) {
    options.data = {
      data: apiEncrypt(encodeObject(options.data), $config.encryptkey),
      s: apiEncrypt(session_id, '1234')
    };
  }

  // SUBMIT
  ajaxSubmit(options);
};

// ======================================================================
// SUBMIT
// ======================================================================

const ajaxSubmit = (options) => {
  console.log('%c--- AJAX ---', 'color:#007257');

  options.retry++;

  // prevent caching
  $.ajaxSetup({ cache: false });

  $.ajax({
    headers: options.headers,
    type: options.method,
    url: options.url,
    data: options.data,
    dataType: options.dataType,
    timeout: options.timeout,
    crossDomain: options.crossdomain,
    async: options.async,
    success: (response) => {
      onSuccess(options, response);
    },
    error: (xhr, status, error) => {
      onError(options, xhr, status, error);
    }
  });
};

// ======================================================================
// ON SUCCESS
// ======================================================================

const onSuccess = (options, response) => {
  // callback
  options.callback(response, options.callbackVars);
  // handle API errors
  if (!options.error && response.error && response.error.length > 0) {
    // show error popup
    showError(response.error);
    // log error
    logError({
      title: 'AJAX Response Error',
      message: '',
      endpoint: options.url
    });
  }
};

// ======================================================================
// ON ERROR
// ======================================================================

const onError = (options, xhr, status, error) => {
  if (options.error == false) {
    if (xhr.status != 200 && options.retry < options.limit) {
      // retry AJAX call
      ajaxSubmit(options);
    } else {
      // timeout error
      if (status == 'timeout') {
        // show error popup
        showError();
        // log to database
        logError({
          title: 'AJAX Timeout Error',
          message: '',
          endpoint: options.url
        });
      }
      // unknown error
      else {
        // show popup
        showError();
        // log to database
        logError({
          title: 'AJAX Unknown Error',
          message: '',
          endpoint: options.url
        });
      }
      // callback
      options.callback({}, options.callbackVars);
    }
  }
};

// ======================================================================
// SHOW ERROR
// ======================================================================

const showError = (apiErrorResponse) => {
  // if (options.showErrors) {
  //   if (typeof apiErrorResponse !== 'undefined') {
  //     var output = '';
  //     for (var i in response.error) {
  //       output += '<br />';
  //       output += response.error[i];
  //       if ($.trim(response.error[i]).slice(-1) != '.') {
  //         output += '.';
  //       }
  //     }
  //     $popups.error('Oops!', 'Sorry, the following errors occurred: ' + output);
  //   } else {
  //     $popups.error('Oops!', 'A timeout error has occurred.<br />Please refresh the page to try again.');
  //   }
  // }
};

// ======================================================================
// LOG ERROR
// ======================================================================

const logError = () => {
  // if (options.logErrors) {
  //   console.log('%c### ERROR LOG ###', 'color:#F00;');
  //   var d = {
  //     // identity: (typeof($m.user)!=='undefined' && typeof($m.user.identity)!=='undefined' && $m.user.identity!=null?$m.user.identity:''),
  //     endpoint: 'Unknown',
  //     title: 'Unknown Error',
  //     message: ''
  //   };
  //   $.extend(d, details);
  //   ajax({
  //     url: $config.api_root + 'app.logError',
  //     method: 'POST',
  //     error: true,
  //     data: d,
  //     callback: function (response) {
  //       console.log(response);
  //     }
  //   });
  // }
};

// ======================================================================
// EXPORT
// ======================================================================

export default ajax;
