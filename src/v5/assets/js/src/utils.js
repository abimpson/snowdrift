// ======================================================================
// FEATURE DETECTION
// ======================================================================

export const detectModernBrowser = () => {
  return 'querySelector' in document && 'localStorage' in window && 'addEventListener' in window;
};

export const detectLocalStorage = () => {
  return window.localStorage ? true : false;
};

export const detectSessionStorage = () => {
  return window.sessionStorage ? true : false;
};

export const detectPushState = () => {
  return !!(window.history && history.pushState);
};

export const detectReplaceState = () => {
  return !!(window.history && history.replaceState);
};

export const detectTouchScreen = () => {
  try {
    document.createEvent('TouchEvent');
    return true;
  } catch (e) {
    return false;
  }
};

export const detectCssFeatures = () => {
  // position sticky
  try {
    $device.css.positionSticky = (function () {
      var el = document.createElement('a'),
        mStyle = el.style;
      mStyle.cssText = 'position:sticky;position:-webkit-sticky;position:-ms-sticky;';
      var supportsPositionSticky = mStyle.position.indexOf('sticky') !== -1;
      if (supportsPositionSticky) {
        $el('body').addClass('css-position-sticky');
      }
      return supportsPositionSticky;
    })();
  } catch (e) {}
};

// ======================================================================
// BLOCKER
// ======================================================================

export const showBlocker = () => {
  $('#blocker').addClass('visible');
};

export const hideBlocker = () => {
  $('#blocker').removeClass('visible');
};

// ======================================================================
// LOCAL STORAGE
// ======================================================================

export const localStorageSet = (key, val) => {
  if (detectLocalStorage()) {
    try {
      localStorage.setItem(key, JSON.stringify(val));
    } catch (e) {}
  }
};

export const localStorageGet = (key) => {
  if (detectLocalStorage()) {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      return null;
    }
  } else {
    return null;
  }
};

export const localStorageUnset = (key) => {
  if (detectLocalStorage()) {
    try {
      localStorage.removeItem(key);
    } catch (e) {
      return null;
    }
  }
};

export const localStorageClear = () => {
  if (detectLocalStorage()) {
    try {
      localStorage.clear();
    } catch (e) {
      return null;
    }
  }
};

// ======================================================================
// SCROLL
// ======================================================================

export const scrollToPosition = (position, duration) => {
  if (typeof duration === 'number' && duration > 0) {
    $('html, body').animate(
      {
        scrollTop: position
      },
      duration
    );
  } else {
    window.scrollTo(0, position);
  }
};

export const scrollToTop = (duration) => {
  scrollToPosition(0, duration);
};

export const scrollToElement = (element, duration) => {
  var top = element.offset().top;
  scrollToPosition(0, duration);
};
