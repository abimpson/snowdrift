// ======================================================================
// IMPORTS
// ======================================================================

import * as utils from './utils';

// ======================================================================
// STATE
// ======================================================================

var submitted = false;
var submit_id = null;

// ======================================================================
// INIT
// ======================================================================

export const init = () => {
  // SUBMIT
  $('body').on('submit', 'form', function () {
    return submitForm($(this));
  });

  // VALIDATION
  $('body').on('blur change', '[data-validate]', function () {
    validateField($(this));
  });
  $('body').on('click focus blur change', '.js-error', function () {
    $(this).removeClass('js-error');
  });

  // RANGE
  // $('.form__range').each(function () {
  //   var slider = $(this).find('input[type=range]');
  //   var input = $(this).find('input[type=text]');
  //   slider.on('change', function () {
  //     var val = $(this).val();
  //     var max = $(this).attr('max');
  //     if (val == max) {
  //       val = 'Any';
  //     } else {
  //       var multiplier = $(this).attr('data-multiplier');
  //       if (typeof multiplier === 'string') {
  //         val = val * multiplier;
  //       }
  //     }
  //     input.val(val);
  //   });
  // });

  // SELECT
  // $('body').on('click', '.form__select-head', function () {
  //   var parent = $(this).parents('.form__select');
  //   if (parent.hasClass('form__select--closed')) {
  //     closeSelects();
  //     parent.removeClass('form__select--closed');
  //     parent.addClass('form__select--open');
  //     parent.find('.form__select-options').scrollTop(0);
  //   } else {
  //     parent.removeClass('form__select--open');
  //     parent.addClass('form__select--closed');
  //   }
  //   return false;
  // });
  // $('body').on('click', '.form__select-option', function () {
  //   var parent = $(this).parents('.form__select');
  //   var head = parent.find('.form__select-head');
  //   var input = parent.find('select');
  //   var key = $(this).attr('data-val');
  //   var val = $(this).text();
  //   head.text(val);
  //   input.val(key);
  //   // toggle
  //   parent.removeClass('form__select--open');
  //   parent.addClass('form__select--closed');
  //   return false;
  // });

  // MULTI SELECT
  // $('body').on('click', '.form__multi-option', function () {
  //   var parent = $(this).parents('.form__select');
  //   var head = parent.find('.form__select-head');
  //   var input = parent.find('select');
  //   var key = $(this).attr('data-val');
  //   // toggle
  //   if (!$(this).hasClass('form__multi-option--checked')) {
  //     $(this).addClass('form__multi-option--checked');
  //     input.find('option[value="' + key + '"]').prop('selected', true);
  //   } else {
  //     $(this).removeClass('form__multi-option--checked');
  //     input.find('option[value="' + key + '"]').prop('selected', false);
  //   }
  //   // output total
  //   var total = parent.find('.form__multi-option--checked').length;
  //   if (total > 0) {
  //     head.text(total + ' Selected');
  //   } else {
  //     head.text('Any');
  //   }
  //   return false;
  // });
};

// ======================================================================
// RESET
// ======================================================================

// const closeSelects = () => {
//   var elements = $('.form__select--open');
//   elements.removeClass('form__select--open');
//   elements.addClass('form__select--closed');
// };

const hideErrors = () => {
  $('.popup .js-error').removeClass('js-error');
  $('.form__row--error').hide();
};

const resetAll = () => {
  // closeSelects();
  hideErrors();
};

// ======================================================================
// VALIDATE
// ======================================================================

const validateForm = (form) => {
  console.log('%c--- forms - validateForm ---', 'color:#b91782;');
  var errors = 0;
  var fields = form.find('[data-validate]');
  fields.each(function () {
    if (!validateField($(this))) {
      errors++;
    }
  });
  return errors == 0 ? true : false;
};

const validateField = (element) => {
  console.log('%c--- forms - validateField ---', 'color:#b91782;');
  var type = element.attr('data-validate');
  // validate length
  if (type == 'text' || type == 'email' || type == 'phone') {
    var val = element.val();
    var min = 1;
    var max = element.attr('maxlength');
    if (typeof max === 'undefined') {
      max = 80;
    }
    if (!isValidLengthRange(val, min, max)) {
      element.addClass('js-error');
      return false;
    }
  }
  // validate email
  if (type == 'email') {
    if (!isValidEmail(val)) {
      element.addClass('js-error');
      return false;
    }
  }
  return true;
};

// ======================================================================
// SUBMIT
// ======================================================================

const submitForm = (form) => {
  console.log('%c--- forms - submitForm ---', 'color:#b91782;');
  // validate
  var valid = validateForm(form);
  if (!valid) {
    form.find('.form__row--error').show();
    utils.hideBlocker();
    return false;
  }
  // return
  return true;
  // log form id
  // submit_id = form_id;
  // recaptcha
  // grecaptcha.execute();
};

// const submitRecaptcha = () => {
//   console.log('%c--- forms - submitRecaptcha - ' + submit_id + ' ---', 'color:#b91782;');
//   // show blockers
//   utils.showBlocker();
//   // get form data
//   var form = $('form#' + submit_id);
//   var action = form.attr('action');
//   var data = form.serialize();
//   // submit data
//   $ajax({
//     url: action,
//     data: data,
//     method: 'POST',
//     dataType: 'html',
//     callback: submitCallback
//   });
//   // save state
//   submitted = true;
// };

// const submitCallback = (response) => {
//   console.log('%c--- forms - submitCallback ---', 'color:#b91782;');
//   console.log(response);
//   $popups.show('enquire-thanks');
//   utils.hideBlocker();
// };
