<?php



// ======================================================================
// ENVIRONMENTAL CONFIG
// ======================================================================

if(strpos($_SERVER['SERVER_NAME'],'.test')!==false){
	// LOCAL
	$server = 'local';
	$cdn = '';
	$root = '/src/v5/';
	$version = '?v=' . rand(9999,99999999999);
}else{
	// LIVE
	$server = 'live';
	$cdn = '';
	$root = '/';
	$version = '?v=' . 1;
}

// ======================================================================
// STANDARD INCLUDES
// ======================================================================

include("assets/php/standard.php");

// ======================================================================
// FUNCTIONS
// ======================================================================

require_once('_functions.php');



?><!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TILE -->
	<title>Site Start</title>

	<!-- META -->
	<meta name="keywords" content="">
	<meta name="description" content="">

  <?php /*
	<!-- META - FACEBOOK OPEN GRAPH -->
	<meta property="fb:app_id" content="">
	<meta property="og:site_name" content="" />
	<meta property="og:type" content="" />
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:url" content="" />
	<meta property="og:image" content="">
  */ ?>
  
  <?php /*
	<!-- META - TWITTER CARD -->
	<meta name="twitter:card" content="">
	<meta name="twitter:site" content="">
	<meta name="twitter:creator" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:image" content="">
  */ ?>
  
  <?php /*
	<!-- FAVICONS -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $cdn.$root; ?>favicon.ico?">
	<link rel="icon" type="image/x-icon" href="<?php echo $cdn.$root; ?>favicon.ico">
	<link rel="mask-icon" href="<?php echo $cdn.$root; ?>assets/favicons/favicon.svg" color="#fc25a2">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $cdn.$root; ?>assets/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $cdn.$root; ?>assets/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $cdn.$root; ?>assets/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $cdn.$root; ?>assets/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $cdn.$root; ?>assets/favicons/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="<?php echo $cdn.$root; ?>assets/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#000000">
  */ ?>

	<!-- MOBILE -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- DEVICE DETECT -->
	<?php $device = deviceDetect(); echo $device['js']; ?>

	<!-- CSS -->
	<link href="<?php echo $root; ?>assets/css/dist/styles.css<?php echo $version; ?>" rel="stylesheet" type="text/css">

	<!-- MODERNIZR -->
	<script src="<?php echo $root; ?>assets/js/vendor/modernizr.js<?php echo $version; ?>"></script>

	<!-- JQUERY -->
	<script src="<?php echo $root; ?>assets/js/vendor/jquery-3.4.1.min.js"></script>

	<!-- JS -->
	<script src="<?php echo $root; ?>assets/js/dist/vendor.min.js<?php echo $version; ?>"></script>
	<script src="<?php echo $root; ?>assets/js/dist/scripts.min.js<?php echo $version; ?>"></script>
	
	
	
</head>
<body class="<?php echo $device['type']; ?>">
	
	
	
	<header></header>
  
  <main>