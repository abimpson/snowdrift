<?php

/*!

	(C) KERVE
	
	AUTHOR: 	ALEX BIMPSON
	NAME: 		PHP STANDARD FUNCTIONS
	VERSION:	1.7
	UPDATED:	2019-05-14
	
*/

// ======================================================================
// INCLUDES
// ======================================================================

include("standard.mobiledetect.php");

// ======================================================================
// ERROR REPORTING
// ======================================================================

function errors($show=true,$deprecated=false){
	if($show==true){
		if($deprecated==false){
			error_reporting(E_ALL);
		}else{
			error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
		}
		ini_set('display_errors', 1);
	}else{
		error_reporting(0);
		ini_set('display_errors', 0);
	};
}

// ======================================================================
// PRINT PRE
// ======================================================================

function print_pre($data){
	echo '<pre>';
		print_r($data);
	echo '</pre>';
}

// ======================================================================
// DEV DETECT
// ======================================================================

function devDetect($ip_array){
	//IP ADDRESS
	$ip = (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	$ips = explode(',',$ip);
	//CHECK FOR IP IN ARRAY
	$return = false;
	foreach($ips as $key1 => $val1){
		foreach($ip_array as $key => $val){
			if($val == $val1){
				$return = true;
			}
		}
	}
	return $return;
}

// ======================================================================
// URL VARIABLE DATA
// ======================================================================

function urlData(){
	if(!empty($_GET) && json_encode($_GET)){
		$return = 'var URL_DATA = {
			set: true,
			data: '.json_encode($_GET).'
		};';
	}else{
		$return = 'var URL_DATA = {
			set: false
		};';
	}
	return $return;
}

// ======================================================================
// FACEBOOK APP DATA
// ======================================================================

function fbAppData(){
	global $signed_request;
	if(!empty($signed_request) && !empty($signed_request->app_data) && json_decode($signed_request->app_data)){
		$app_data = $signed_request->app_data;
	}elseif(isset($_GET['app_data']) && json_decode(urldecode(stripslashes($_GET['app_data'])))){
		$app_data = urldecode(stripslashes($_GET['app_data']));
	}
	if(isset($app_data) && $app_data!=''){
		$return = 'var APP_DATA = {
			set: true,
			data: '.$app_data.'
		};';
	}else{
		$return = 'var APP_DATA = {
			set: false
		};';
	}
	return $return;
}

// ======================================================================
// FACEBOOK APP REQUESTS
// ======================================================================

function fbAppRequests(){
	if(isset($_GET['request_ids'])){
		$app_request = explode(',',$_GET['request_ids']);
		$return = 'var APP_REQUESTS = {
			set: true,
			data: '.json_encode($app_request).'
		};';
	}else{
		$return = 'var APP_REQUESTS = {
			set: false
		};';
	}
	return $return;
}

?>