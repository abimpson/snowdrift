<?php

function deviceDetect(){
	
	// ======================================================================
	// INIT
	// ======================================================================
	
	//setup
	$device = array();
	//user agent
	$device['user_agent'] = (!empty($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : getenv('HTTP_USER_AGENT');
	//check for windows
	if(strpos($device['user_agent'], "Win") !== FALSE){
		$device['windows'] = true;
	}else{
		$device['windows'] = false;
	}
	//check for webkit
	$DEVICE['webkit'] = (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'webkit') !== false ? true : false);
	//check for <= IE9
	if(isset($_GET['ie9']) || preg_match('/(?i)msie [5-9]/',$device['user_agent'])){
		$device['ie9'] = true;
	}else{
		$device['ie9'] = false;
	}
	//check for <= IE8
	if(isset($_GET['ie8']) || preg_match('/(?i)msie [5-8]/',$device['user_agent'])){
		$device['ie8'] = true;
	}else{
		$device['ie8'] = false;
	}
	//check for <= IE7
	if(isset($_GET['ie7']) || preg_match('/(?i)msie [5-7]/',$device['user_agent'])){
		$device['ie7'] = true;
	}else{
		$device['ie7'] = false;
	}
	//include mobile detect class - http://mobiledetect.net
	if (!class_exists('Mobile_Detect')) {
		require('Mobile_Detect.php');
	}
	$mobiledetect = new Mobile_Detect();
	//include desktop detect class - https://github.com/garetjax/phpbrowscap
	//timezone
	date_default_timezone_set("Europe/London");
	
	// ======================================================================
	// VARS
	// ======================================================================
	
	//type
	$device['type'] = 'Unknown';
	//touchscreen
	$device['touchscreen'] = 'Unknown';
	//os
	$device['os'] = 'Unknown';
	$device['os_version'] = 'Unknown';
	//browser
	$device['browser'] = 'Unknown';
	$device['browser_version'] = 'Unknown';
	//device
	$device['mobile_device'] = 'Unknown';
	
	// ======================================================================
	// DEVICE DETECT
	// ======================================================================
	
	if($mobiledetect->isTablet()){
		$device['type'] = 'tablet';
	}else if($mobiledetect->isMobile()){
		$device['type'] = 'mobile';
	}else{
		$device['type'] = 'desktop';
	}
	
	// ======================================================================
	// TOUCHSCREEN DETECT
	// ======================================================================
	
	if($device['type'] == 'mobile' || $device['type'] == 'tablet'){
		$device['touchscreen'] = true;
		/*echo '<script type="text/javascript"> var TOUCHSCREEN = true; </script>';*/
	}else{
		$device['touchscreen'] = false;
		/*echo '<script type="text/javascript"> var TOUCHSCREEN = false; </script>';*/
	}
	
	// ======================================================================
	// MOBILE STUFF
	// ======================================================================
	
	if($device['type'] == 'mobile' || $device['type'] == 'tablet'){
		
		// MOBILE BROWSER DETECT
		if( $mobiledetect->isBlazer() ){
			$device['browser'] = 'Blazer';
		}else if( $mobiledetect->isBolt() ){
			$device['browser'] = 'Bolt';
		}else if( $mobiledetect->isChrome() ){
			$device['browser'] = 'Chrome';
		}else if( $mobiledetect->isDiigoBrowser() ){
			$device['browser'] = 'Diigo';
		}else if( $mobiledetect->isDolfin() ){
			$device['browser'] = 'Dolfin';
		}else if( $mobiledetect->isFirefox() ){
			$device['browser'] = 'Firefox';
		}else if( $mobiledetect->isIE() ){
			$device['browser'] = 'IE';
		}else if( $mobiledetect->isMercury() ){
			$device['browser'] = 'Mercury';
		}else if( $mobiledetect->isOpera() ){
			$device['browser'] = 'Opera';
		}else if( $mobiledetect->isPuffin() ){
			$device['browser'] = 'Puffin';
		}else if( $mobiledetect->isSafari() ){
			$device['browser'] = 'Safari';
		}else if( $mobiledetect->isSkyfire() ){
			$device['browser'] = 'Skyfire';
		}else if( $mobiledetect->isTeaShark() ){
			$device['browser'] = 'Teashark';
		}else if( $mobiledetect->isTizen() ){
			$device['browser'] = 'Tizen';
		}else if( $mobiledetect->isUCBrowser() ){
			$device['browser'] = 'UCBrowser';
		}
		
		// MOBILE OS DETECT
		if( $mobiledetect->isAndroidOS() ){
			$device['os'] = 'Android';
			if($mobiledetect->version('Android')){
				$device['os_version'] = str_replace('_','.',$mobiledetect->version('Android'));
			}
		}else if( $mobiledetect->isbadaOS() ){
			$device['os'] = 'BadaOS';
		}else if( $mobiledetect->isBlackBerryOS() ){
			$device['os'] = 'Blackberry';
			if($mobiledetect->version('BlackBerry')){
				$device['os_version'] = str_replace('_','.',$mobiledetect->version('BlackBerry'));
			}
		}else if( $mobiledetect->isBREWOS() ){
			$device['os'] = 'BREWOS';
		}else if( $mobiledetect->isiOS() ){
			$device['os'] = 'iOS';
			if($mobiledetect->version('iOS')){
				$device['os_version'] = str_replace('_','.',$mobiledetect->version('iOS'));
			}
		}else if( $mobiledetect->isJavaOS() ){
			$device['os'] = 'JavaOS';
		}else if( $mobiledetect->isMaemoOS() ){
			$device['os'] = 'MaemoOS';
		}else if( $mobiledetect->isMeeGoOS() ){
			$device['os'] = 'MeeGoOS';
		}else if( $mobiledetect->isPalmOS() ){
			$device['os'] = 'PalmOS';
		}else if( $mobiledetect->isSymbianOS() ){
			$device['os'] = 'Symbian';
		}else if( $mobiledetect->iswebOS() ){
			$device['os'] = 'WebOS';
		}else if( $mobiledetect->isWindowsMobileOS() ){
			$device['os'] = 'WindowsMobile';
		}else if( $mobiledetect->isWindowsPhoneOS() ){
			$device['os'] = 'WindowsPhone';
		}
		
		// MOBILE DEVICE DETECT
		if($mobiledetect->isiPhone()){
			$device['mobile_device'] = 'iPhone';
		}else if($mobiledetect->isiPad()){
			$device['mobile_device'] = 'iPad';
		}
		
	}
	
	// ======================================================================
	// OUTPUT JS OBJECT
	// ======================================================================
	
	$device['js'] = '<script type="text/javascript">
		var DEVICE = {
			type: "'.$device['type'].'",
			touchscreen: '.($device['touchscreen']==true?'true':'false').',
			mobile_device: "'.$device['mobile_device'].'",
			os: "'.$device['os'].'",
			os_version: "'.$device['os_version'].'",
			browser: "'.$device['browser'].'",
			browser_version: "'.$device['browser_version'].'"
		};
	</script>';
	//user_agent: "'.$device['user_agent'].'",
	return $device;
	
}



?>