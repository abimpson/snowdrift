var $docsize = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  use: true,
  freq: 100,

  // ======================================================================
  // DATA
  // ======================================================================

  timeout: null,
  w: 0,
  h: 0,
  r: 0,

  // ======================================================================
  // RUN
  // ======================================================================

  run: function(force) {
    if ($docsize.use == true) {
      if (force == true) {
        $docsize.func(force);
      } else {
        clearTimeout($docsize.timeout);
        $docsize.timeout = setTimeout(function() {
          $docsize.func(force);
        }, $docsize.freq);
      }
    }
  },
  func: function(force) {
    console.log('%c--- docsize ---', 'color:#ddc000');
    var oldWidth = $docsize.w;
    var oldHeight = $docsize.h;
    $docsize.w = $(document).width();
    $docsize.h = $(document).height();
    $docsize.r = $docsize.w / $docsize.h;
    if (force == true) {
      $docsize.w_func();
      $docsize.h_func();
      $docsize.x_func();
    } else {
      if ($docsize.w != oldWidth) {
        $docsize.w_func();
      }
      if ($docsize.h != oldHeight) {
        $docsize.h_func();
      }
      if ($docsize.w != oldWidth || $docsize.h != oldHeight) {
        $docsize.x_func();
      }
    }
  },

  // ======================================================================
  // CALLBACK FUNCTIONS
  // ======================================================================

  w_func: function() {},
  h_func: function() {},
  x_func: function() {}
};
