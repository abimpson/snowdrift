var $preload = {
  // ======================================================================
  // CONFIG
  // ======================================================================

  method: 1,
  fontcss: ['assets/css/styles.css'],

  // ======================================================================
  // VARS
  // ======================================================================

  total: 0,
  done: 0,
  fonts_loaded: false,
  timeout: 5000,

  // ======================================================================
  // LIST FILES TO PRELOAD
  // ======================================================================

  // FONTS
  fonts: Array(),
  // ALL
  files: Array(),
  // ALL - NON RETINA
  files_non_retina: Array(),
  // ALL - RETINA
  files_retina: Array(),
  // DESKTOP
  files_d: Array(),
  // DESKTOP - NON RETINA
  files_d_non_retina: Array(),
  // DESKTOP - RETINA
  files_d_retina: Array(),
  // MOBILE
  files_m: Array(),
  // MOBILE - NON RETINA
  files_m_non_retina: Array(),
  // MOBILE - RETINA
  files_m_retina: Array(),
  // DEMAND FILES
  files_demand: Array(),

  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {
    if (!$m.device.ie8) {
      // EXTEND
      if (typeof $config_preload !== 'undefined') {
        $.extend($preload, $config_preload);
      }
      // FONTS
      $preload.loadfonts();
      // ALL
      for (var i in $preload.files) {
        $preload.preload($preload.files[i]);
      }
      if ($('#retina').is(':visible')) {
        for (var j in $preload.files_retina) {
          $preload.preload($preload.files_retina[j]);
        }
      } else {
        for (var k in $preload.files_non_retina) {
          $preload.preload($preload.files_non_retina[k]);
        }
      }
      // DESKTOP
      if ($m.device.type != 'Mobile') {
        for (var m in $preload.files_d) {
          $preload.preload($preload.files_d[m]);
        }
      }
      if ($m.device.type != 'Mobile' && $('#retina').is(':visible')) {
        for (var n in $preload.files_d_retina) {
          $preload.preload($preload.files_d_retina[n]);
        }
      } else if ($m.device.type != 'Mobile') {
        for (var o in $preload.files_d_non_retina) {
          $preload.preload($preload.files_d_non_retina[o]);
        }
      }
      // MOBILE
      if ($m.device.type == 'Mobile') {
        for (var p in $preload.files_m) {
          $preload.preload($preload.files_m[p]);
        }
      }
      if ($m.device.type == 'Mobile' && $('#retina').is(':visible')) {
        for (var q in $preload.files_m_retina) {
          $preload.preload($preload.files_m_retina[q]);
        }
      } else if ($m.device.type == 'Mobile') {
        for (var r in $preload.files_m_non_retina) {
          $preload.preload($preload.files_m_non_retina[r]);
        }
      }
      // TIMEOUT FALLBACK
      setTimeout(function() {
        $preload.done = $preload.total;
        $preload.loaded_fonts = true;
      }, $preload.timeout);
    }
  },

  // ======================================================================
  // PRELOAD FILES
  // ======================================================================

  preload: function(i) {
    $preload.total++;
    if ($preload.method == 1) {
      img = new Image();
      img.src = i;
      img.onload = function() {
        $preload.done++;
        console.log('%c--- preload - ' + $preload.done + '/' + $preload.total + ' - ' + i + ' ---', 'color:#aaa');
      };
    } else {
      var myImge = $('<img />').attr('src', i);
      $(myImge).load(function() {
        $preload.done++;
        console.log('%c--- preload - ' + $preload.done + '/' + $preload.total + ' - ' + i + ' ---', 'color:#aaa');
      });
    }
  },

  // ======================================================================
  // PRELOAD FILES ON DEMAND
  // ======================================================================

  demand: function(file_array) {
    for (var i in file_array) {
      $preload.files_demand.push(file_array[i]);
      $preload.preload(file_array[i]);
    }
  },

  // ======================================================================
  // PRELOAD FONTS
  // ======================================================================

  loadfonts: function() {
    if ($preload.fonts.length > 0 && $m.device.type != 'mobile') {
      WebFont.load({
        custom: {
          families: $preload.fonts,
          urls: $preload.fontcss
        },
        active: function() {
          console.log('%c--- preload - fonts loaded ---', 'color:#8FC685');
          $preload.fonts_loaded = true;
        },
        inactive: function() {
          console.log('%c--- preload - fonts failed to load ---', 'color:##8FC685');
        }
      });
    }
  }
};
