var $wp = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  el: {
    posts: '.js-wp-posts', // contains the posts
    loading: '.js-wp-loading', // loader animation
    trigger: '.js-wp-trigger' // triggers autoload when it scrolls into view
    // more: '.js-wp-more', // "load more" button (optional)
  },
  masonryClass: 'js-masonry-grid',

  // ======================================================================
  // DATA
  // ======================================================================

  inited: false, // store whether code has been inited
  masonry: false, // store whether container uses masonry
  path: null, // store current AJAX path
  pathQuery: '', // store current path query
  page: 1, // store current page
  last: false, // is this the last page?
  count: 0, // count to ensure correct data is shown
  loading: false, // is a page loading?

  // ======================================================================
  // INIT
  // ======================================================================

  init: function () {
    console.log('%c--- wordpress - init ---', 'color:#6317b9;');
    if ($($wp.el.posts).length > 0) {
      // get current page path
      $wp.getPath();
      // check for masonry
      $wp.masonry = $($wp.el.posts).hasClass($wp.masonryClass);
      // load more on click
      // $($wp.el.more).click(function() {
      //   $wp.loadMore();
      //   return false;
      // });
      // inited
      $wp.inited = true;
    }
  },

  // ======================================================================
  // GET CURRENT PATH (URL)
  // ======================================================================

  getPath: function () {
    // get path
    var path = window.location.href;
    // get search vars
    path = path.split('?');
    var query = path.length > 1 ? path[1] : '';
    path = path[0];
    // remove pagination
    path = path.split('/page');
    path = path[0];
    // remove trailing slash
    if (path.substr(path.length - 1) == '/') {
      path = path.substring(0, path.length - 1);
    }
    // save
    $wp.path = path;
    $wp.pathQuery = query;
  },

  // ======================================================================
  // PAGE SCROLL - LOAD MORE IF REQUIRED
  // ======================================================================

  scroll: function (currentScrollPos) {
    if ($wp.inited) {
      var trigger = $($wp.el.trigger).offset().top;
      var pos = currentScrollPos + $windowsize.h;
      if (pos >= trigger) {
        $wp.loadMore();
      }
    }
  },

  // ======================================================================
  // LOAD MORE
  // ======================================================================

  loadMore: function () {
    if (!$wp.last && !$wp.loading) {
      console.log('%c--- wordpress - loadMore ---', 'color:#6317b9');
      $wp.loading = true;
      // hide more button
      // $($wp.el.more).hide();
      // show loader
      $($wp.el.loading).show();
      // get path
      var path = $wp.path;
      // append page
      $wp.page++;
      path = path + '/page/' + $wp.page;
      if ($wp.pathQuery == '') {
        path = path + '/?ajax=true';
      } else {
        path = path + '/?' + $wp.pathQuery + '&ajax=true';
      }
      // ajax
      $wp.ajax(path);
    }
  },

  // ======================================================================
  // AJAX
  // ======================================================================

  ajax: function (path, replace) {
    console.log('%c--- wordpress - ajax ---', 'color:#6317b9');
    console.log(path);
    $wp.count++;
    $ajax({
      url: path,
      data: {},
      method: 'GET',
      dataType: 'html',
      callback: $wp.ajaxCallback,
      callback_vars: {
        count: $wp.count,
        replace: replace
      }
    });
  },

  // ======================================================================
  // AJAX CALLBACK
  // ======================================================================

  ajaxCallback: function (response, vars) {
    console.log('%c--- wordpress - callback ---', 'color:#6317b9');
    // ensure this isn't an old callback
    if ($wp.count == vars.count) {
      // hide loader
      $($wp.el.loading).hide();
      // get new content
      var items = $(response).find($wp.el.posts).children();
      // check if new content exists
      if (items.length < 1) {
        console.log('%c--- wordpress - no more items ---', 'color:#6317b9');
        // save that there are no more items
        $wp.last = true;
      } else {
        $wp.last = false;
        // append new items
        if (vars.replace === true) {
          $wp.page = 1;
          $($wp.el.posts).html(items);
          if ($wp.masonry) {
            $masonry.reloadItems($($wp.el.posts));
          }
        } else {
          $($wp.el.posts).append(items);
          if ($wp.masonry) {
            $masonry.appendItems($($wp.el.posts), items);
          }
        }
        // show more button
        // $($wp.el.more).show();
      }
      // re-enable
      clearTimeout($wp.timeout);
      $wp.timeout = setTimeout(function () {
        $wp.loading = false;
      }, 500);
    }
  }
};
