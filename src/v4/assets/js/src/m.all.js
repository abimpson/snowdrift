// ======================================================================
// INIT
// ======================================================================

var $m = $m || {};

// ======================================================================
// APP
// ======================================================================

$m.app = {
  ready: false,
  loaded: false
};

// ======================================================================
// DEVICE
// ======================================================================

$m.device = {
  type: '',
  retina: false,
  mustard: false,
  pushstate: false,
  replacestate: false,
  localstorage: window.localStorage ? true : false,
  sessionstorage: window.sessionStorage ? true : false,
  ie9: false,
  ie8: false,
  ie7: false,
  ie6: false
};
$m.device_update = function() {
  // IE
  if (yourBrowser == 'IE') {
    if (yourBrowserVersion < 10) {
      $m.device.ie9 = true;
    }
    if (yourBrowserVersion < 9) {
      $m.device.ie8 = true;
    }
    if (yourBrowserVersion < 8) {
      $m.device.ie7 = true;
    }
    if (yourBrowserVersion < 7) {
      $m.device.ie6 = true;
    }
  }
  // DEVICE
  if (typeof DEVICE !== 'undefined') {
    $.extend($m.device, DEVICE);
  }
  // RETINA
  if (typeof $m !== 'undefined' && $('#retina').is(':visible')) {
    $m.device.retina = true;
  }
  // PUSHSTATE
  if (!!(window.history && history.pushState)) {
    $m.device.pushstate = true;
  }
  if (!!(window.history && history.replaceState)) {
    $m.device.replacestate = true;
  }
  // CUT THE MUSTARD - TEST FOR MODERN HTML 5 JS BROWSERS
  if ('querySelector' in document && 'localStorage' in window && 'addEventListener' in window) {
    $m.device.mustard = true;
  }
};

// ======================================================================
// URL DATA
// ======================================================================

$m.url_data = {
  set: false
};
$m.url_data_update = function() {
  if (typeof URL_DATA !== 'undefined') {
    $.extend($m.url_data, URL_DATA);
  }
};

// ======================================================================
// FACEBOOK APP DATA
// ======================================================================

// $m.app_data = {
//   set: false
// };
// $m.app_data_update = function() {
//   if (typeof APP_DATA !== 'undefined') {
//     $.extend($m.app_data, APP_DATA);
//   }
// };

// ======================================================================
// FACEBOOK APP REQUESTS
// ======================================================================

// $m.app_requests = {
//   set: false
// };
// $m.app_requests_update = function() {
//   if (typeof APP_REQUESTS !== 'undefined') {
//     $.extend($m.app_requests, APP_REQUESTS);
//     $m.app_requests_get();
//   }
// };
// $m.app_requests_get = function() {
//   if ($facebook.got_user == true) {
//     for (var i in $m.app_requests.data) {
//       FB.api(
//         '/' + $m.app_requests.data[i],
//         'get',
//         {
//           // ?access_token=
//           // access_token: $facebook.auth_response.accessToken
//         },
//         function(response) {
//           console.log(response);
//         }
//       );
//     }
//   } else {
//     setTimeout(function() {
//       $m.app_requests_get();
//     }, 50);
//   }
// };

// ======================================================================
// USER
// ======================================================================

// $m.user = {
//   identity: null,
//   confirmed: 0,
//   verified: 0
// };
// $m.user_update = function() {};

// ======================================================================
// INVITER
// ======================================================================

// $m.inviter = {
//   set: false,
//   identity: null
// };
// $m.inviter_update = function(data) {};

// ======================================================================
// INVITEES
// ======================================================================

// $m.invitees = {
//   set: false
// };
// $m.invitees_update = function(data) {};
