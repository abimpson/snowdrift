var $masonry = {
  // ======================================================================
  // OPTIONS
  // ======================================================================

  el: {
    wrap: '.js-masonry-grid', // masonry grid wrapper
    items: '.js-masonry-item' // individual masonry items
  },

  // ======================================================================
  // INIT
  // ======================================================================

  init: function () {
    console.log('%c--- masonry - init ---', 'color:#1f77b8;');
    if (typeof Masonry !== 'undefined' && $($masonry.el.wrap).length > 0) {
      $($masonry.el.wrap).each(function () {
        var self = $(this);
        element = self.masonry({
          itemSelector: $masonry.el.items,
          transitionDuration: 0
        });
        $masonry.imagesLoaded(self);
      });
    }
  },

  // ======================================================================
  // POPUP - INSTANT RESIZE
  // ======================================================================

  popupResize: function (element) {
    console.log('%c--- masonry - popupResize ---', 'color:#1f77b8;');
    element.find($masonry.el.wrap).masonry('layout');
  },

  // ======================================================================
  // HELPER FUNCTIONS
  // ======================================================================

  reloadItems: function (wrap) {
    wrap.masonry('reloadItems');
    $masonry.imagesLoaded(wrap);
  },

  appendItems: function (wrap, items) {
    wrap.masonry('appended', items);
    $masonry.imagesLoaded(wrap);
  },

  imagesLoaded: function (wrap) {
    wrap.imagesLoaded().progress(function () {
      console.log('image loaded 2');
      wrap.masonry('layout');
    });
  }

  // ======================================================================
  // RESIZE
  // ======================================================================

  // resize: function () {
  //   if ($masonry.ready) {
  //     $masonry.element.masonry('layout');
  //     $masonry.element.imagesLoaded().progress(function () {
  //       $masonry.element.masonry('layout');
  //     });
  //   }
  // }
};
