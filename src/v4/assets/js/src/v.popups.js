var $popup = $popup || {};
var $popups = {
  // ======================================================================
  // INIT
  // ======================================================================

  init: function() {},

  // ======================================================================
  // VARS
  // ======================================================================

  current: '',

  // ======================================================================
  // SHOW POPUP
  // ======================================================================

  show: function(popup, vars) {
    console.log('%c--- popup - ' + popup + ' ---', 'color:#E1BF97;');
    // SET CURRENT
    $popups.current = popup;
    // HIDE EXISTING POPUPS
    $('.popup').hide();
    // SHOW POPUP
    if ($m.device.ie8) {
      $('#popup_' + popup).show();
      $('#popups')
        .stop()
        .show();
    } else {
      $('#popup_' + popup).show();
      $('#popups')
        .stop()
        .fadeIn(300);
    }
    // SCROLL TO TOP
    if ($m.device.type == 'mobile') {
      $app.scroll_top();
    }
    // POPUP START
    $popups.start($popups.current);
    // ANALYTICS
    $analytics.google.page('/popup/' + popup);
  },

  // ======================================================================
  // HIDE ALL POPUPS
  // ======================================================================

  hide: function() {
    if ($m.device.ie8) {
      $('#popups').hide();
    } else {
      $('#popups').fadeOut(300);
    }
  },

  // ======================================================================
  // ERROR POPUP
  // ======================================================================

  error: function(title, msg) {
    $('#error_title').text(title);
    $('#error_msg').text(msg);
    $popups.show('error');
  },

  // ======================================================================
  // START POPUP FUNCTIONS
  // ======================================================================

  start: function(popup) {
    if (!$el('#popup_' + popup).hasClass('loading')) {
      // RESIZE
      $popups.resize();
      // POPUP SPECIFIC START
      if (hasKey($popup, popup + '.start')) {
        $popup[popup].start();
        $popup[popup].started = true;
      }
    } else {
      clearTimeout($popups.timeout_run);
      $popups.timeout_run = setTimeout(function() {
        $popups.start(popup);
      }, 10);
    }
  },

  // ======================================================================
  // RESIZE POPUP FUNCTIONS
  // ======================================================================

  resize: function() {}
};
