

var ap = ap || {};
ap.nav = {
	
	
	/******************** INIT ********************/
	init: function(){
		ap.nav.nav();
		ap.nav.pages();
		ap.nav.popups();
		ap.nav.touchswipe();
	},
	
	
	/******************** MAIN NAV ********************/
	nav: function(){
		//PAGES
		$('body').on('click','.btn_page',function(){
			var page = $(this).attr('data-page');
			if(page && typeof(page)!=='undefined'){
				ap.pages.show(page);
			};
			return false;
		});
		//POPUPS
		$('body').on('click','.btn_popup',function(){
			var popup = $(this).attr('data-popup');
			if(popup && typeof(popup)!=='undefined'){
				ap.popups.show(popup);
			};
			return false;
		});
	},
	
	
	/******************** PAGES ********************/
	pages: function(){
		//PAGE SIZING
		$('#btn_size_s').click(function(){
			$('.container').removeAttr('style');
			return false;
		});
		$('#btn_size_m').click(function(){
			$('.container').css('min-height',900);
			return false;
		});
		$('#btn_size_l').click(function(){
			$('.container').css('min-height',1200);
			return false;
		});
		//FACEBOOK - CANVAS
		$('body').on('click','#btn_canvas_info',function(){
			FACEBOOK.canvas_get_info(callback);
			return false;
		});
		$('body').on('click','#btn_canvas_autosize',function(){
			FACEBOOK.canvas_set_autosize();
			return false;
		});
		$('body').on('click','#btn_canvas_setsize',function(){
			//FACEBOOK.canvas_set_size(500,810);
			FACEBOOK.canvas_set_size(8000);
			return false;
		});
		$('body').on('click','#btn_canvas_scrolltop',function(){
			FACEBOOK.canvas_scroll_top();
			return false;
		});
		$('body').on('click','#btn_canvas_scrollto',function(){
			FACEBOOK.canvas_scroll_to(200);
			return false;
		});
		//FACEBOOK - AUTHENTICATE
		$('body').on('click','#btn_login',function(){
			FACEBOOK.login(ap.all.callback);
			return false;
		});
		//FACEBOOK - REVOKE PERMISSIONS
		$('body').on('click','#btn_revoke',function(){
			FACEBOOK.revoke();
			return false;
		});
		//FACEBOOK - LOGOUT
		$('body').on('click','#btn_logout',function(){
			FACEBOOK.logout();
			return false;
		});
		//FACEBOOK - ADD ADDITIONAL PERMISSIONS
		$('body').on('click','#btn_permissions',function(){
			FACEBOOK.add_perms('publish_actions',ap.all.callback);
			return false;
		});
		//FACEBOOK - GET APP FRIENDS
		$('body').on('click','#btn_getappfriends',function(){
			FACEBOOK.get_app_friends(ap.all.callback);
			return false;
		});
		//FACEBOOK - SHARE - TIMELINE
		$('body').on('click','#btn_share_feed',function(){
			ap.share.fb_timeline();
			return false;
		});
		//FACEBOOK - SHARE - FRIENDS TIMELINE
		$('body').on('click','#btn_share_feed2',function(){
			ap.share.fb_timeline('100003990067778');
			return false;
		});
		//FACEBOOK - SHARE - MESSAGE
		$('body').on('click','#btn_share_message',function(){
			ap.share.fb_message();
			return false;
		});
		//FACEBOOK - SHARE - APP REQUEST
		$('body').on('click','#btn_share_request',function(){
			ap.share.fb_request();
			return false;
		});
		//FACEBOOK - SHARE - POST TO WALL
		$('body').on('click','#btn_fb_post',function(){
			ap.share.fb_post();
			return false;
		});
		//FACEBOOK - SHARE - POST PHOTO
		$('body').on('click','#btn_fb_photo',function(){
			ap.share.fb_photo();
			return false;
		});
		//FACEBOOK - FRIEND REQUEST
		$('body').on('click','#btn_friend_request',function(){
			FACEBOOK.friend_request('100003990067778');
			return false;
		});
		//FACEBOOK - ACTION
		$('body').on('click','#btn_publish_action',function(){
			ap.share.fb_action();
			return false;
		});
		//FACEBOOK - NOTIFICATION
		$('body').on('click','#btn_notification',function(){
			ap.share.fb_notification();
			return false;
		});
		//FACEBOOK - ADD TAB
		$('body').on('click','#btn_add_tab',function(){
			FB.ui({
				method:			'pagetab',
				app_id:			'634951309860775',
				redirect_uri:	'http://facebook.kerve.com/story_westfield/'
			}, function(response){
				console.log(response);
			});
			return false;
		});
		//TWITTER
		$('#btn_twitter_share').click(function(){
			TWITTER.share('http://www.kerve.co.uk','Hello There');
			return false;
		});
		$('#btn_twitter_auth').click(function(){
			TWITTER.authenticate();
			return false;
		});
		$('#btn_twitter_user_timeline').click(function(){
			TWITTER.get_user_timeline();
			return false;
		});
		$('#btn_twitter_mentions_timeline').click(function(){
			TWITTER.get_mentions_timeline();
			return false;
		});
		//MUSIC
		$('#btn_music_spotify').click(function(){
			MUSIC.search_spotify('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_itunes').click(function(){
			MUSIC.search_itunes('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_lastfm').click(function(){
			MUSIC.search_lastfm('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_rdio').click(function(){
			MUSIC.search_rdio('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_deezer').click(function(){
			MUSIC.search_deezer('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_grooveshark').click(function(){
			MUSIC.search_grooveshark('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_musicbrainz').click(function(){
			MUSIC.search_musicbrainz('queen',ap.all.callback);
			return false;
		});
		$('#btn_music_decibel').click(function(){
			MUSIC.search_decibel('queen',ap.all.callback);
			return false;
		});
		//YOUTUBE
		$('#btn_youtube_search').click(function(){
			YOUTUBE.search_youtube('blurred lines',ap.all.callback);
			return false;
		});
		//BITLY
		$('#btn_bitly_shorten').click(function(){
			$bitly.shorten('https://apps.facebook.com/theheatapp/?app_data=%7B%22id%22%3A2553376095%7D',ap.all.callback);
			return false;
		});
		$('#btn_bitly_expand').click(function(){
			$bitly.expand('http://bit.ly/IgooZ0',ap.all.callback);
			return false;
		});
	},
	
	
	/******************** POPUPS ********************/
	popups: function() {
		//CLOSE
		$('body').on('click','.btn_close_popup, .btn_close_popup2, .blackout',function(){
			ap.popups.hide();
			return false;
		});
	},
	
	
	/******************** TOUCHSWIPE ********************/
	touchswipe: function(){
		if(!ap.m.device.ie8){
			//GAME
			/*$("#page_game").swipe({
				swipeRight: function(){
					$('#game_slider').prrpleSliderLeft()
				},
				swipeLeft: function(){
					$('#game_slider').prrpleSliderRight()
				},
				threshold:100,
				excludedElements: ''
			});*/
		};
	}


};

