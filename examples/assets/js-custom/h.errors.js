// JavaScript Document


var ap = ap || {};
ap.errors = {

	
	/******************** CONFIG ********************/
	show_generic: true,		//whether to show generic error messages
	show_response: true,	//whether to show response error messages
	show_timeout: true,		//whether to show timeout error messages
	show_ajax: true,		//whether to show AJAX error messages
	log_errors: true,		//whether to log errors in the database
	
	
	/******************** INIT ********************/
	init: function(){
		//EXTEND CONFIG
		$.extend(ap.errors, ap.config_errors);
	},
	
	
	/******************** ERROR - GENERIC ********************/
	generic_error: function(text){
		console.log('%c### ERROR - GENERIC ###','color:#F00;');
		if(ap.errors.show_generic == true){
			if (!text) {
				text = 'Sorry! It looks like something\'s gone wrong. Please refresh your browser window and try again.';
			};
			$('#error_message').html(text);
			ap.popups.show('error');
		};
		ap.all.hide_loader();
	},
	
	
	/******************** ERROR - RESPONSE ********************/
	response_error: function(response){
		console.log('%c### ERROR - RESPONSE ###','color:#F00;');
		console.log(response);
		if(ap.errors.show_response == true){
			var output = '';
			for(var i in response.error){
				output += response.error[i];
				if($.trim(response.error[i]).slice(-1) != '.'){
					output += '.';
				}
				output += '<br />';
			}
			$('#error_message').html(output);
			ap.popups.show('error');
		};
		ap.all.hide_loader();
	},
	
	
	/******************** ERROR - TIMEOUT ********************/
	timeout_error: function(url){
		console.log('%c### ERROR - TIMEOUT ###','color:#F00;');
		console.log(url);
		if(ap.errors.show_timeout == true){
			$('#error_message').html('A timeout error occurred - '+url+'.<br />Please refresh the page to try again.');
			ap.popups.show('error');
		};
		ap.all.hide_loader();
	},
	
	
	/******************** ERROR - AJAX ********************/
	ajax_error: function(url){
		console.log('%c### ERROR - AJAX ###','color:#F00;');
		console.log(url);
		if(ap.errors.show_ajax == true){
			$('#error_message').html('An AJAX error occurred - '+url+'.<br />Please refresh the page to try again.');
			ap.popups.show('error');
		};
		ap.all.hide_loader();
	},
	
	
	/******************** ERROR - LOG TO DATABASE ********************/
	log_error: function(details){
		if(ap.errors.log_errors == true){
			console.log('%c### ERROR LOG ###','color:#F00;');
			try{
				//DETAILS
				var d = {
					identity: (typeof(ap.m.user)!=='undefined' && typeof(ap.m.user.identity)!=='undefined' && ap.m.user.identity!=null?ap.m.user.identity:''),
					endpoint: 'Unknown',
					title: 'Unknown Error',
					message: ''
				};
				$.extend(d,details);
				console.log(d);
				//AJAX
				ap.ajax({
					url: ap.config.api_root+'app.logError',
					data: d,
					callback: ap.errors.log_error_callback,
					method: 'GET',
					error: true
				});
			}catch(e){};
		};
	},
	log_error_callback: function(response){
		console.log(response);
	},
	log_error_test: function(){
		ap.errors.log_error({
			endpoint: 'testError',
			title: 'This is a test error log',
			message: '' //JSON.stringify({test:"test"})
		});
	}


};
