

/*
	(C) KERVE
	
	AUTHOR:		ALEX BIMPSON
	NAME:		TWITTER JS APP FRAMEWORK
	VERSION:	1.2
	UPDATED:	2014-10-25
*/


/********************************************************************************************************************************/
/****************************************************** TWITTER - FRAMEWORK *****************************************************/
/********************************************************************************************************************************/

var TWITTER = new Object({
	
	
	/******************** CONFIG ********************/
	path_to_api: '/examples/assets/php/twitter',
	
	
	/******************** VARIABLES ********************/
	user: null,
	
	
	/******************** AUTHORISE ********************/
	authenticate: function(){
		if(window.location.protocol == 'https:'){
			var secure = true;
		}else{
			var secure = false;
		}
		window.open(TWITTER.path_to_api+'/twitter.auth.php?secure='+secure);
	},
	
	
	/******************** AUTHORISATION CALLBACK ********************/
	authenticate_callback: function(response){
		console.log(response);
		if(response.success == true){
			TWITTER.user = {
				username: response.username
			};
			TWITTER.twit_click_auth();
		}else{
			TWITTER.twit_cancelled();
		};
	},
	
	
	/******************** AUTH CALLBACKS ********************/
	twit_click_pre: function(){},
	twit_click_auth: function(){},
	twit_cancelled: function(){},
	
	
	/******************** FUNCTIONS - GET ********************/
	//MENTIONS TIMELINE
	get_user_timeline: function(){
		ap.ajax({
			url: TWITTER.path_to_api+'/twitter.functions.php',
			data: {
					query: 'get_user_timeline',
					user_id: '65347826'
				},
			callback: TWITTER.get_user_timeline_callback,
			method: 'GET'
		});
	},
	get_user_timeline_callback: function(response){
		console.log(response);
	},
	//MENTIONS TIMELINE
	get_mentions_timeline: function(){
		ap.ajax({
			url: TWITTER.path_to_api+'/twitter.functions.php',
			data: {
					query: 'get_mentions_timeline'
				},
			callback: TWITTER.get_mentions_timeline_callback,
			method: 'GET'
		});
	},
	get_mentions_timeline_callback: function(response){
		console.log(response);
	},
	
	
	/******************** TWEET DIALOG ********************/
	share: function(url,text){
		if(typeof(url) == "string" && typeof(text) == "string"){
			window.open('https://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text),'_blank');
		}else if(typeof(url) == "string"){
			window.open('https://twitter.com/share?url='+encodeURIComponent(url),'_blank');
		}else if(typeof(text) == "string"){
			window.open('https://twitter.com/share?text='+encodeURIComponent(text),'_blank');
		}else{
			window.open('https://twitter.com/share','_blank');
		};
	}
	
	
});





/********************************************************************************************************************************/
/***************************************************** TWITTER - INITIALISE SDK *************************************************/
/********************************************************************************************************************************/


/******************** LOGIN BUTTON ********************/
$(function(){
	$('.twitter_login').click(function(){
		TWITTER.twit_click_pre();
		TWITTER.authenticate();
		return false;
	});
});


/******************** EXTEND ********************/
$(document).ready(function(){
	//EXTEND CONFIG
	if(typeof TWITTER_CONFIG !== 'undefined' && TWITTER_CONFIG != null){
		$.extend(TWITTER, TWITTER_CONFIG);
	};
	if(typeof TWITTER_FUNCTIONS !== 'undefined' && TWITTER_FUNCTIONS != null){
		$.extend(TWITTER, TWITTER_FUNCTIONS);
	};
});



