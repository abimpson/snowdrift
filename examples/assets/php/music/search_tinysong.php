<?php


/********** HEADERS **********/
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){
	
}else{
	header("content-type: application/json; charset=utf8");
}
//DEFINE OUTPUT ARRAY
$myArray = Array();







/********** PROCESS **********/
//tinysong key f5aab848b5fcea68f20e4f227f893b0b
if(isset($_GET['song'])){
	//URL
	$url = "http://tinysong.com/s/".$_GET['song']."?format=json&key=f5aab848b5fcea68f20e4f227f893b0b";
	//Curl query
	$ch = curl_init($url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch,CURLOPT_FAILONERROR,true);
	//curl_setopt($ch,CURLOPT_ERRORBUFFER,true);
	if(curl_exec($ch) === false) {
		$myArray['success'] = 0;
		$myArray['message'] = 'Curl error';
	} else {
		$response = json_decode(curl_exec($ch));
		$myArray['success'] = 1;
		$myArray['response'] = $response;
	}
	curl_close ($ch);
}else{
	$myArray['success'] = 0;
	$myArray['message'] = 'No song parameter supplied';
}










/********** OUTPUT **********/
echo json_encode($myArray);




?>