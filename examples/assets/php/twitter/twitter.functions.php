<?php



/*

See documentation at https://dev.twitter.com/docs/api/1.1

//example calls once authenticated
$account = $connection->get('statuses/mentions_timeline');
$status = $connection->post('statuses/update', array('status' => 'Text of status here', 'in_reply_to_status_id' => 123456));
$status = $connection->delete('statuses/destroy/12345');

*/



/********** JSON HEADER **********/
header('Content-Type: application/json');


/********** CONFIG **********/
require_once('config.php');
$response = array();


/********** CONNECT **********/
//Build TwitterOAuth object with client credentials
//NOTE - currently connecting with unauthorised credentials provided by the app
$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET);


/********** FUNCTIONS **********/
if(isset($_GET['query']) && $_GET['query'] == 'get_mentions_timeline'){
	$response['method'] = 'get_mentions_timeline';
	$response['success'] = true;
	$response['query'] = $connection->get('statuses/mentions_timeline');
}elseif(isset($_GET['query']) && $_GET['query'] == 'get_user_timeline'){
	$response['method'] = 'get_user_timeline';
	$response['success'] = true;
	$response['query'] = $connection->get('statuses/user_timeline',array('user_id'=>65347826));
}else{
	$response['success'] = false;
}
echo json_encode($response);



?>