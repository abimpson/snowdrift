<?php



/********** CONFIG **********/
require_once('config.php');


/********** TWITTER SUCCESS **********/
if(isset($_GET['denied'])){
	$success = false;
}else{
	$success = true;
}


/********** TWITTER CALLBACk **********/
if($success == true){
		
	//Build TwitterOAuth object with client credentials
	$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
	
	//Now we ask Twitter for long lasting token credentials. These are specific to the application and user and will act like password to make future requests. Normally the token credentials would get saved in your database but for this example we are just using sessions
	$token_credentials = $connection->getAccessToken($_REQUEST['oauth_verifier']);
	
	
	/*
	//Save temporary credentials to session
	$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
	$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
	
	//With the token credentials we build a new TwitterOAuth object
	$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
	*/
	
	
	//With the token credentials we build a new TwitterOAuth object
	$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, $token_credentials['oauth_token'], $token_credentials['oauth_token_secret']);
	
	//And finally we can make requests authenticated as the user. You can GET, POST, and DELETE API methods. Directly copy the path from the API documentation and add an array of any parameter you wish to include for the API method such as curser or in_reply_to_status_id
	$account = $connection->get('account/verify_credentials');
	
	?>
	<script>
		window.opener.TWITTER.authenticate_callback({success:true,username:'<?php echo $account->screen_name; ?>'});
		window.close();
	</script>
	<?php
	
}else{
	
	?>
	<script>
		window.opener.TWITTER.authenticate_callback({success:false});
		window.close();
	</script>
	<?php
	
}



?>