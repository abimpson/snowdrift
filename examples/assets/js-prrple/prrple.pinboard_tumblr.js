// JavaScript Document



/********************************************************************/
/***************************** PINBOARD *****************************/
/********************************************************************/


var PINBOARD_ADJUSTING = false;
(function($){
	$.fn.pinBoard = function(options){
		
		/********** CONFIG *********/
		var DEFAULTS = {
			col_width:		200,	//width of columns
			v_spacing:		15,		//spacing between items
			h_spacing:		15,		//spacing between items
			side_spaces:	true	//include spacing on outer edges
		};
		if(options){
			$.extend(DEFAULTS, options);
		}
		var PINBOARD = $(this);
		var NO_OF_COLS = 0;
		var COL_ARRAY = new Array();
		
		
		
		/********** INIT *********/
		$(document).ready(function(){
			//SET COL WIDTHS
			$('.pin_item').each(function(){
				$(this).css('width',DEFAULTS.col_width);
			});
			//POSITION COLS
			adjustLayout();
		});
		$(window).resize(function(){
			adjustLayout();
		});
		
		
		/********** CREATE LAYOUT *********/
		function adjustLayout(loop){
			if(PINBOARD_ADJUSTING==true){
				if(!loop){loop=0;}
				if(loop<2){
					loop++;
					setTimeout(function(){
						adjustLayout(loop);
					},600);
				}
			}else{
				//TIMERS
				PINBOARD_ADJUSTING = true;
				setTimeout(function(){
					PINBOARD_ADJUSTING = false;
				},500);
				
				//CALCULATE NO OF COLS
				var maxWidth = PINBOARD.parent().width();
				var noOfCols = (maxWidth/DEFAULTS.col_width).toString();
				var noOfCols2 = noOfCols.split('.');
				var noOfCols3 = parseInt(noOfCols2[0]);
				var totalWidth = (noOfCols3 * DEFAULTS.col_width) + (noOfCols3 * DEFAULTS.h_spacing) + (DEFAULTS.side_spaces==true?+DEFAULTS.h_spacing:-DEFAULTS.h_spacing);
				if(totalWidth > maxWidth){
					NO_OF_COLS = noOfCols3-1;
				}else{
					NO_OF_COLS = noOfCols3;
				}
				
				//CREATE COLUMN ARRAY
				COL_ARRAY = new Array();
				for(i=0;i<NO_OF_COLS;i++){
					COL_ARRAY[i]=0;
				}
				
				//POSITION EACH pin_item
				$('.pin_item').each(function(){
					//GET SHORTEST COL
					var height = $(this).height();
					var minCol = 0;
					var minHeight = 0;
					for(var i in COL_ARRAY){
						if(i == 0 || COL_ARRAY[i] < minHeight){
							minHeight = COL_ARRAY[i];
							minCol = i;
						}
					}
					//UPDATE ARRAY
					COL_ARRAY[minCol] = COL_ARRAY[minCol] + DEFAULTS.v_spacing + height;
					//CALCULATE NEW POSITION
					var x = (minCol * DEFAULTS.col_width) + ((minCol-1)*DEFAULTS.h_spacing) + DEFAULTS.h_spacing;
					var y = minHeight;
					$(this).css({
						'left':x,
						'top':y
					});
				});
				
				//CALCULATE PINBOARD DIMENSIONS
				var maxCol = 0;
				var maxHeight = 0;
				for(var i in COL_ARRAY){
					if(i == 0 || COL_ARRAY[i] > maxHeight){
						maxHeight = COL_ARRAY[i];
					}
				}
				
				//UPDATE PINBOARD DIMENSIONS
				var actual_no_of_cols = $('.pin_item').length;
				var possible_no_of_cols = NO_OF_COLS;
				if(NO_OF_COLS > actual_no_of_cols){
					NO_OF_COLS = actual_no_of_cols;
				}
				var newWidth = (NO_OF_COLS * DEFAULTS.col_width) + (NO_OF_COLS * DEFAULTS.h_spacing) - DEFAULTS.h_spacing;
				var newWidth2 = (possible_no_of_cols * DEFAULTS.col_width) + (possible_no_of_cols * DEFAULTS.h_spacing) - DEFAULTS.h_spacing;
				PINBOARD.css({
					'height':maxHeight-DEFAULTS.v_spacing,
					'width':newWidth
				});
				
				//UPDATE TUMBLR STUFF
				$('#likewrapper').css({
					'width':newWidth2
				});
				$('ul#likes li').css({
					'width':((newWidth2 - (9 * DEFAULTS.h_spacing))/10)-30,
					'margin-right':DEFAULTS.h_spacing
				});
				$('ul#likes li:last').css({
					'margin-right':0
				});
			}
		}
	}
})(jQuery);


/********** TUMBLR STUFF *********/
function moreNotes(){
	$('.more_notes_link').click(function(){
		createPinboard();
		moreNotes();
		setTimeout(function(){
			createPinboard();
			moreNotes();
			moreImages();
		},1000);
	});
}
function moreImages(){
	$("img").one('load', function() {
		setTimeout(function(){
			createPinboard();
		},300);
	}).each(function() {
		if(this.complete) $(this).load();
	});
}


/********** INIT PINBOARD *********/
$(document).ready(function(){
	createPinboard();
	moreNotes();
	moreImages();
});










/********************************************************************/
/************************* INFINITE SCROLLER ************************/
/********************************************************************/


/*
var tumblrAutoPager={
	url:"http://proto.jp/",
	ver:"0.1.7",
	rF:true,
	gP:{},
	pp:null,
	ppId:"",
	LN:location.hostname,
	init:function(){
		if($("autopagerize_icon")||navigator.userAgent.indexOf('iPhone')!=-1)return;
		var tAP=tumblrAutoPager;var p=1;var lh=location.href;var lhp=lh.lastIndexOf("/page/");var lht=lh.lastIndexOf("/tagged/");
		if(lhp!=-1){p=parseInt(lh.slice(lhp+6));tAP.LN=lh.slice(7,lhp);}else if(lht!=-1){tAP.LN=lh.slice(7);if(tAP.LN.slice(tAP.LN.length-1)=="/")tAP.LN=tAP.LN.slice(0,tAP.LN.length-1);}else if("http://"+tAP.LN+"/"!=lh){return;};
		var gPFncs=[];
		gPFncs[0]=function(aE){var r=[];for(var i=0,l=aE.length;i<l;i++){if(aE[i].className=="autopagerize_page_element"){r=gCE(aE[i]);break;}}return r;};
		gPFncs[1]=function(aE){var r=[];for(var i=0,l=aE.length;i<l;i++){var arr=aE[i].className?aE[i].className.split(" "):null;if(arr){for(var j=0;j<arr.length;j++){arr[j]=="post"?r.push(aE[i]):null;}}}return r;};
		gPFncs[2]=function(aE){var r=[];var tmpId=tAP.ppId?[tAP.ppId]:["posts","main","container","content","apDiv2","wrapper","projects"];for(var i=0,l=aE.length;i<l;i++){for(var j=0;j<tmpId.length;j++){if(aE[i].id==tmpId[j]){r=gCE(aE[i]);tAP.ppId=aE[i].id;break;}}}return r;};
		for(var i=0;i<gPFncs.length;i++){var getElems=gPFncs[i](document.body.getElementsByTagName('*'));if(getElems.length){tAP.gP=gPFncs[i];tAP.pp=getElems[0].parentNode;break;}}
		function gCE(pElem){var r=[];for(var i=0,l=pElem.childNodes.length;i<l;i++){r.push(pElem.childNodes.item(i))}return r;}
		if(!tAP.pp){return;}
		sendRequest.README={license:'Public Domain',url:'http://jsgt.org/lib/ajax/ref.htm',version:0.516,author:'Toshiro Takahashi'};
		
		function chkAjaBrowser(){var A,B=navigator.userAgent;this.bw={safari:((A=B.split('AppleWebKit/')[1])?A.split('(')[0].split('.')[0]:0)>=124,konqueror:((A=B.split('Konqueror/')[1])?A.split(';')[0]:0)>=3.3,mozes:((A=B.split('Gecko/')[1])?A.split(' ')[0]:0)>=20011128,opera:(!!window.opera)&&((typeof XMLHttpRequest)=='function'),msie:(!!window.ActiveXObject)?(!!createHttpRequest()):false};return(this.bw.safari||this.bw.konqueror||this.bw.mozes||this.bw.opera||this.bw.msie)}
			
		function createHttpRequest(){if(window.XMLHttpRequest){return new XMLHttpRequest()}else{if(window.ActiveXObject){try{return new ActiveXObject('Msxml2.XMLHTTP')}catch(B){try{return new ActiveXObject('Microsoft.XMLHTTP')}catch(A){return null}}}else{return null}}};
		
		function sendRequest(E,R,C,D,F,G,S,A){
			var Q=C.toUpperCase()=='GET',H=createHttpRequest();
			if(H==null){return null}
			if((G)?G:false){
				D+=((D.indexOf('?')==-1)?'?':'&')+'t='+(new Date()).getTime()
			}
			var P=new chkAjaBrowser(),L=P.bw.opera,I=P.bw.safari,N=P.bw.konqueror,M=P.bw.mozes;
			if(typeof E=='object'){
				var J=E.onload;
				var O=E.onbeforsetheader
			}else{
				var J=E;var O=null
			}
			if(L||I||M){
				H.onload=function(){
					J(H);H.abort()
				}
			}else{
				H.onreadystatechange=function(){
					if(H.readyState==4){
						J(H);H.abort()
					}
				}
			}
			R=K(R,D);
			if(Q){
				D+=((D.indexOf('?')==-1)?'?':(R=='')?'':'&')+R
			}
			H.open(C,D,F,S,A);
			if(!!O){O(H)}
			B(H);H.send(R);
			function B(T){if(!L||typeof T.setRequestHeader=='function'){T.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8')}return T}
			function K(X,V){
				var Z=[];
				if(typeof X=='object'){
					for(var W in X){
						Y(W,X[W])
					}
				}else{
					if(typeof X=='string'){
						if(X==''){return''}
						if(X.charAt(0)=='&'){X=X.substring(1,X.length)}
						var T=X.split('&');
						for(var W=0;W<T.length;W++){
							var U=T[W].split('=');Y(U[0],U[1])
						}
					}
				}
				function Y(b,a){Z.push(encodeURIComponent(b)+'='+encodeURIComponent(a))}
				return Z.join('&')
			}
			return H
		}
	
		function addNextPage(oj){
			//MOD
			console.log('ADDING PAGE');
			createPinboard();
			moreImages();
			
			if(oj.status==404){
				tAP.remainFlg=false;return;
				//MOD
				console.log('ERROR');
			}
			var d=document.createElement("div");
			d.innerHTML=oj.responseText;
			var posts=tAP.gP(d.getElementsByTagName("*"));
			if(posts.length<2){tAP.rF=false;return;}
			d=document.createElement("div");
			d.className="tumblrAutoPager_page_info";
			tAP.pp.appendChild(d);
			for(var i=0;i<posts.length;i++){tAP.pp.appendChild(posts[i]);}
			var footer=$("footer");footer?footer.parentNode.appendChild(footer):null;tAP.rF=true;
		}
		
		watch_scroll();
		function watch_scroll(){
			var d=document.compatMode=="BackCompat"?document.body:document.documentElement;
			var r=d.scrollHeight-d.clientHeight-(d.scrollTop||document.body.scrollTop);
			if(r<d.clientHeight*2&&tAP.rF){
				tAP.rF=false;
				p++;
				sendRequest(addNextPage,"","GET","http://"+tAP.LN+"/page/"+p,true);
			}
			setTimeout(arguments.callee,200);
		};
		function $(id){
			return document.getElementById(id)
		};
	},
	switchAutoPage:function(){
		this.rF=!this.rF;
		var aE=document.getElementsByTagName('*');
		for(var i=0,l=aE.length;i<l;i++){
			if(aE[i].className=="tAP_switch"){
				aE[i].firstChild.nodeValue=this.rF?"AutoPage[OFF]":"AutoPage[ON]";
			}
		}
	}
};

window.addEventListener?window.addEventListener('load',tumblrAutoPager.init,false):window.attachEvent?window.attachEvent("onload",tumblrAutoPager.init):window.onload=tumblrAutoPager.init;


/**/