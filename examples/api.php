<?php
	
	
	
	/***** CUSTOM PHP *****/
	require_once('../web/v4/assets/php/standard.php');
	errors();
	
	
	/********** CONFIG - GLOBAL **********/
	$config = array();
	$config['appId'] = '314847395261211';
	$config['secret'] = '2450d35b0d01e60ed2a3442737611a2a';
	$url = "https://graph.facebook.com/oauth/access_token?"
	. "client_id=" . $config['appId']
	. "&client_secret=" . $config['secret']
	. "&grant_type=client_credentials";
	
	
	/********** GET APP ACCESS TOKEN - CURL **********/
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch,CURLOPT_FAILONERROR,true);
	curl_exec($ch);
	$result = str_replace('access_token=','',curl_exec($ch));
	curl_close($ch);
	
	
	
?><!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel = "shortcut icon" type = "image/x-icon" href = "/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
	
	<!-- MOBILE -->
	<?php $device = deviceDetect(); echo $device['js']; ?>
	
	<!-- MODERNIZR -->
	<script src="assets/js-plugins/modernizr.js"></script>
	
	<!-- HTML5 SHIV -->
	<!--[if lt IE 9]>
	<script src="assets/js-plugins/html5shiv.js"></script>
	<![endif]-->
	
	<!-- JS VARS -->
	<script type="text/javascript">
		<?php echo fbAppData(); ?>
		<?php echo fbAppRequests(); ?>
		//ACCESS TOKEN
		var APP_ACCESS_TOKEN = '<?php echo $result; ?>';
	</script>
	
	<!-- JQUERY -->
	<script src="assets/js-plugins/jquery.1.11.1.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js-custom/_config.js"></script>
	<script src="assets/js-custom/_init.js"></script>
	<script src="assets/js-custom/a.all.js"></script>
	<script src="assets/js-custom/a.init.js"></script>
	<script src="assets/js-custom/a.share.js"></script>
	<script src="assets/js-custom/b.windowsize.js"></script>
	<script src="assets/js-custom/c.nav.js"></script>
	<script src="assets/js-custom/h.ajax.js"></script>
	<script src="assets/js-custom/h.errors.js"></script>
	<script src="assets/js-custom/m.all.js"></script>
	<script src="assets/js-custom/s.all.js"></script>
	<script src="assets/js-custom/x.facebook.3.js"></script>
	<script src="assets/js-custom/x.twitter.js"></script>
	<script src="assets/js-custom/x.youtube.js"></script>
	<script src="assets/js-custom/x.music.js"></script>
	<script src="assets/js-custom/x.bitly.js"></script>
	
	
	
</head>
<body>
	<div id="fb-root"></div>
	
	
	
	<!-- HEADER -->
	<?php require_once('_header.php'); ?>
	
	
	
	<!-- CONTENT -->
	<div id="content">
		
		<div class="container">
			<h1>Set Page Size</h1>
			<p><a id="btn_size_s" href="">Page Size - Small</a></p>
			<p><a id="btn_size_m" href="">Page Size - Med</a></p>
			<p><a id="btn_size_l" href="">Page Size - Large</a></p>
		</div>
		
		<div class="container">
			<h1>Facebook</h1>
			<table style="width:100%;">
				<tr>
					<td style="vertical-align:top;width:50%;">
						<p><a id="btn_login" href="">Authenticate</a></p>
						<p><a id="btn_revoke" href="">Revoke Permissions</a></p>
						<p><a id="btn_logout" href="">Logout of Facebook</a></p>
						<p><a id="btn_permissions" href="">Add Additional Permissions</a></p>
						<p>&nbsp;</p>
						<p><a id="btn_canvas_info" href="">Canvas Info</a></p>
						<p><a id="btn_canvas_autosize" href="">Canvas Auto Size</a> (note - doesn't shrink, only grows)</p>
						<p><a id="btn_canvas_setsize" href="">Canvas Set Size</a></p>
						<p><a id="btn_canvas_scrolltop" href="">Canvas Scroll Top</a></p>
						<p><a id="btn_canvas_scrollto" href="">Canvas Scroll To</a></p>
						<p>&nbsp;</p>
						<p><a id="btn_getappfriends" href="">Get App Friends</a></p>
					</td>
					<td style="vertical-align:top;">
						<p><a id="btn_share_feed" href="">Share - Timeline</a></p>
						<p><a id="btn_share_feed2" href="">Share - Friend's Timeline</a></p>
						<p><a id="btn_share_message" href="">Share - Message</a></p>
						<p><a id="btn_share_request" href="">Share - App Request</a></p>
						<p>&nbsp;</p>
						<p><a id="btn_fb_post" href="">Post To Timeline</a></p>
						<p><a id="btn_fb_photo" href="">Post Photo</a></p>
						<p>&nbsp;</p>
						<p><a id="btn_friend_request" href="">Friend Request</a></p>
						<p><a id="btn_publish_action" href="">Publish Action</a></p>
						<p><a id="btn_notification" href="">Notification</a></p>
						<p>&nbsp;</p>
						<p><a id="btn_add_tab" href="">Add Tab</a></p>
					</td>
				</tr>
			</table>
		</div>
		
		<!--<div class="container">
			<h1>Twitter</h1>
			<p><a id="btn_twitter_share" href="">Share</a></p>
			<p><a class="twitter_login" href="">Authenticate</a></p>
			<p><a id="btn_twitter_user_timeline" href="">Get User Timeline</a></p>
			<p><a id="btn_twitter_mentions_timeline" href="">Get Mentions Timeline</a></p>
		</div>-->
		
		<div class="container">
			<h1>Bit.ly</h1>
			<p><a id="btn_bitly_shorten" href="">Shorten URL</a></p>
			<p><a id="btn_bitly_expand" href="">Expand URL</a></p>
		</div>
		
		<div class="container">
			<h1>Music</h1>
			<table style="width:100%;">
				<tr>
					<td style="vertical-align:top;width:50%;">
						<p><a id="btn_music_itunes" href="">Search iTunes</a></p>
						<p><a id="btn_music_lastfm" href="">Search LastFM</a></p>
						<p><a id="btn_music_deezer" href="">Search Deezer</a></p>
						<p><a id="btn_music_musicbrainz" href="">Search MusicBrainz</a></p>
						<!--<p><a id="btn_music_spotify" href="">Search Spotify</a></p>-->
						<!--<p><a id="btn_music_rdio" href="">Search Rdio</a></p>-->
						<!--<p><a id="btn_music_grooveshark" href="">Search GrooveShark</a></p>-->
						<!--<p><a id="btn_music_decibel" href="">Search Decibel</a></p>-->
					</td>
				</tr>
			</table>
		</div>
		
		<div class="container">
			<h1>YouTube</h1>
			<p><a id="btn_youtube_search" href="">Search YouTube</a></p>
		</div>
	
	</div>
	
	
	
	<!-- FOOTER -->
	<?php require_once('_footer.php'); ?>
	
	
	
</body>
</html>