<?php
	
	
	
	/***** CONFIG *****/
	$minify = false;
	
	/***** CDN *****/
	$cdn = ''; //locale_setting('app_cdn_url').locale_setting('app_relative_folder');
	$imageroot = $cdn.'/assets/images/';
	
	/***** CUSTOM PHP *****/
	require('assets/php/standard.php');
	errors(true);
	
	/***** DEV IP DETECTION *****/
	$dev = devDetect(array('213.123.135.131','90.209.101.230','::1'));
	
	
	
?><!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift</title>
	
	<!-- OG -->
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	
	<!-- DEVICE DETECT -->
	<?php $device = deviceDetect(); ?>
	<?php echo $device['js']; ?>
	
	<!-- VIEWPORT -->
	<?php if($device['type']=='mobile'){ ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
	<?php }else{ ?>
		<meta name="viewport" content="width=810, user-scalable=yes" />
	<?php } ?>
	
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
	
	<!-- APP DATA -->
	<script type="text/javascript">
		<?php echo urlData(); ?>
		<?php echo fbAppData(); ?>
		<?php echo fbAppRequests(); ?>
		var session_id = '<?php echo session_id(); ?>';
	</script>
	
	<!-- MODERNIZR -->
	<script src="assets/js-plugins/modernizr.js"></script>
	
	<!-- JQUERY -->
	<?php if($device['ie8'] || $device['ie7']){ ?>
		<script src="assets/js-plugins/jquery-1.12.0.min.js"></script>
	<?php }else{ ?>
		<script src="assets/js-plugins/jquery-2.2.0.min.js"></script>
	<?php } ?>
	
	<?php if($minify==true && $dev!=true){ ?>
		<!-- JS -->
		<script src="assets/js/plugins.min.js"></script>
		<script src="assets/js/scripts.min.js"></script>
	<?php }else{ ?>
		<!-- WEBFONT LOADER -->
		<script src="assets/js-plugins/webfontloader.js"></script>
		
		<!-- CUSTOM SCRIPTS -->
		<script src="assets/js-custom/_config.js"></script>
		<script src="assets/js-custom/_init.js"></script>
		<script src="assets/js-custom/a.app.js"></script>
		<script src="assets/js-custom/a.auth.js"></script>
		<script src="assets/js-custom/a.checks.js"></script>
		<script src="assets/js-custom/b.docsize.js"></script>
		<script src="assets/js-custom/b.inputs.js"></script>
		<script src="assets/js-custom/b.nav.js"></script>
		<script src="assets/js-custom/b.scroll.js"></script>
		<script src="assets/js-custom/b.windowsize.js"></script>
		<script src="assets/js-custom/c.plupload.js"></script>
		<script src="assets/js-custom/c.socket.js"></script>
		<script src="assets/js-custom/h.ajax.js"></script>
		<script src="assets/js-custom/h.ajax_errors.js"></script>
		<script src="assets/js-custom/h.analytics.js"></script>
		<script src="assets/js-custom/h.filter.js"></script>
		<script src="assets/js-custom/h.hijax.js"></script>
		<script src="assets/js-custom/h.motion.js"></script>
		<script src="assets/js-custom/h.preload.js"></script>
		<script src="assets/js-custom/h.pushstate.js"></script>
		<script src="assets/js-custom/h.videos.js"></script>
		<script src="assets/js-custom/m.all.js"></script>
		<script src="assets/js-custom/s.all.js"></script>
		<script src="assets/js-custom/s.fastclick.js"></script>
		<script src="assets/js-custom/v.pages.js"></script>
		<script src="assets/js-custom/v.pages_home.js"></script>
		<script src="assets/js-custom/v.popups.js"></script>
		<script src="assets/js-custom/v.social.js"></script>
		<script src="assets/js-custom/v.toggles.js"></script>
		<script src="assets/js-custom/w.all.js"></script>
		<script src="assets/js-custom/x.bitly.js"></script>
		<script src="assets/js-custom/x.facebook.4.js"></script>
		<script src="assets/js-custom/x.gmaps.js"></script>
		<script src="assets/js-custom/x.youtube.js"></script>
	<?php } ?>
	
	
	
</head>
<body>
	<div id="fb-root"></div>
	<div id="mobile"></div>
	<div id="retina"></div>
	
	
	
	<div id="wrapper">
		
		<?php if($device['ie8'] || isset($_GET['upgrade'])){ ?>
			
			<div id="pages">
				<div class="page" id="page_xupgrade">
					<?php include('html-pages/xupgrade.php');?>
				</div>
			</div>
			
		<?php }else if(isset($_GET['closed'])){ ?>
			
			<div id="pages">
				<div class="page" id="page_closed">
					<?php include('html-pages/closed.php');?>
				</div>
			</div>
			
		<?php }else{ ?>
			
			<div id="pages">
				<div class="page" id="page_loading">
					<?php include('html-pages/loading.php'); ?>
				</div>
				<div class="page" id="page_landing">
					<?php include('html-pages/landing.php'); ?>
				</div>
			</div>
			
			<div id="popups">
				<div class="blackout"></div>
				<div class="popup" id="popup_error"><div class="popup_inner">
					<?php include('html-popups/error.php');?>
				</div></div>
			</div>
			
		<?php } ?>
		
	</div>
	
	
	
	<!-- DEVELOPER -->
	<?php if($device['type']!='mobile' && $dev==true){ ?>
		<div id="developer">
			<div class="developer">
				<h1>DEVELOPER CONTROL PANEL</h1>
			</div>
			<div id="console"></div>
			<div class="developer">
				<p>
					<a onclick="FACEBOOK.revoke();return false;">Revoke Facebook Permissions</a>
				</p>
			</div>
			<div class="developer">
				<p>
					<a href="?">Default</a>
					<a href="?closed=true">Closed</a>
					<a href="?ie8=true">IE8</a>
				</p>
			</div>
			<div class="developer">
				<h2>PAGES:</h2>
				<p>
					<a class="btn_page" data-page="loading">Loading</a>
					<a class="btn_page" data-page="landing">Landing</a>
				</p>
				<h2>POPUPS:</h2>
				<p>
					<a class="btn_popup" data-popup="error">Error</a>
					<a class="btn_close_popup">Close Popups</a>
				</p>
			</div>
		</div>
	<?php } ?>
	
	
	
</body>
</html>