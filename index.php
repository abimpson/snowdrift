<?php
	
	
	
	// PARSEDOWN
	require_once('src/v4/assets/php/parsedown.php');
	
	
	
?><!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	
	<!-- TITLE -->
	<title>SnowDrift JS</title>
	
	<!-- VIEWPORT -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=1" />
	
	<!-- STYLES -->
	<link rel="stylesheet" href="/examples/assets/css/styles.css" type="text/css" />
	
	<!-- JQUERY -->
	<script src="examples/assets/js-plugins/jquery-1.12.0.min.js"></script>
	
	<!-- CUSTOM SCRIPTS -->
	<script src="examples/assets/js-custom/_init2.js"></script>
	<script src="examples/assets/js-custom/b.windowsize.js"></script>
	
	
	
</head>
<body>
	<div id="fb-root"></div>
	
	
	
	<!-- HEADER -->
	<?php require_once('examples/_header.php'); ?>
	
	<!-- CONTENT -->
	<div id="content">
		<div class="container">
			<?php
				$md = file_get_contents('README.md');
				$md = explode('---',$md);
				echo Parsedown::instance()
					->setBreaksEnabled(true)
					->text($md[1]);
			?>
		</div>
	</div>
	
	<!-- FOOTER -->
	<?php require_once('examples/_footer.php'); ?>
	
	
	
</body>
</html>